/* Copyright 2018 Ryan "Izzy" Bales
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H

// Defines the keycodes used by our macros in process_record_user
enum custom_keycodes {
    YD68_BT_PWR = SAFE_RANGE,
    YD68_RGB_PWR,
};

// I've removed the 5 keys on the right to make a 60%.
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
    [0] = LAYOUT_meptllc( /* 0: Base */
        KC_GESC, KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,    KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,  KC_BSLS,   XXXXXXX,
        KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,    KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC, KC_RBRC, KC_BSPC,   XXXXXXX,
        KC_LCTL, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,    KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,          KC_ENT,    XXXXXXX,
        KC_LSFT,          KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,    KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT, MO(1),     XXXXXXX,
        KC_LCTL, KC_LALT, KC_LGUI,                            KC_SPC,                    KC_RALT, KC_LEFT, KC_DOWN, KC_UP,   KC_RIGHT,  XXXXXXX
        ),

    [1] = LAYOUT_meptllc( /* 1: FN */
        KC_GRV,  KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,   KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,  _______, XXXXXXX,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,  KC_DEL, XXXXXXX,
        _______, _______, _______, _______, _______, _______, KC_MPRV, KC_VOLD, KC_VOLU, KC_MNXT, _______, _______,          _______, XXXXXXX,
        _______,          _______, _______, _______, _______, _______, _______, KC_MUTE, _______, _______, _______, _______, _______, XXXXXXX,
        _______, _______, _______,                            KC_MPLY,                   _______, KC_HOME, KC_PGDN, KC_PGUP,  KC_END, XXXXXXX
        ),

    [2] = LAYOUT_meptllc( /* 2: Example Layer */
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, XXXXXXX,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, XXXXXXX,
        _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______,          _______, XXXXXXX,
        _______,          _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, _______, XXXXXXX,
        _______, _______, _______,                            _______,                   _______, _______, _______, _______, _______, XXXXXXX
        ),
};

bool process_record_user(uint16_t keycode, keyrecord_t *record) {
    switch (keycode) {
        case YD68_BT_PWR:
            if (record->event.pressed) {
                // when keycode YD68_BT_PWR is pressed
            } else {
                // when keycode YD68_BT_PWR is released
            }
            break;
        case YD68_RGB_PWR:
            if (record->event.pressed) {
                // when keycode YD68_RGB_PWR is pressed
                PORTE ^= (1<<2);
            } else {
                // when keycode YD68_RGB_PWR is released
            }
            break;
    }
    return true;
}

void matrix_init_user(void) {

}

void matrix_scan_user(void) {

}

void led_set_user(uint8_t usb_led) {

}
